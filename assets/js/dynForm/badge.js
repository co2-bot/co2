dynForm = {
    jsonSchema : {
	    title : "Add a badge",
	    icon : " bookmark-o",
	    type : "object",
	    onLoads : {
	    	onload : function(){
    		 	dyFInputs.setSub("bg-azure");
    		}
	    },
        beforeBuild : function(){
            //alert("before Build orga");
            dyFObj.setMongoId('badges', function(){
                uploadObj.gotoUrl = '/co2/badges';
            });
        },
        afterSave : function(data,callB){
            if( $(uploadObj.domTarget).fineUploader('getUploads').length > 0 ){
                $(uploadObj.domTarget).fineUploader('uploadStoredFiles');   
            }
            window.location.reload();
            
        },
	    properties : {
            name : dyFInputs.inputText("Name", "Must be short and explicit", { required : true }),
            tag :dyFInputs.tags(),
            icon : dyFInputs.inputText("font awesome icon","fa-user, fa-pencil, fa-clock..."),
            color : dyFInputs.inputText("Color", "hexa or web colors allowed"),
            img : dyFInputs.image()

	    }
	}
};